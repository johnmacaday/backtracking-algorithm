<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Auth::routes();

Route::get('/dashboard/{user}', 'UserController@dashboard')->name('dashboard')->middleware('approved');

Route::get('/survey/create', 'SurveyController@showQuestion')->name('survey.create')->middleware('approved');
Route::post('/survey/store', 'SurveyController@createSurvey')->name('survey.store');

Route::get('/survey', 'SurveyController@index')->name('survey.index')->middleware('approved');
Route::get('/my-surveys/{user}', 'UserController@showSurveys')->name('survey.show')->middleware('approved');
Route::get('/survey/{survey}/answer', 'SurveyController@answerSurvey')->name('survey.answer')->middleware('approved');
Route::post('/survey/answer', 'AnswerController@save');

Route::get('/answers/{survey}', 'AnswerController@show')->middleware('approved');
Route::get('/my-answers/{user}', 'UserController@showAnswers')->name('answers.show')->middleware('approved');
Route::get('/join-survey', 'AnswerController@joinSurvey')->name('survey.join')->middleware('approved');
Route::post('/survey/join','AnswerController@surveyJoin')->name('join.survey');

Route::get('/approval', function(){
	return view('approval.show');
})->name('approval');

Route::get('/admin/dashboard', 'AdminController@dashboard')->name('admin.dashboard')->middleware('admin');
Route::post('/user/approve', 'AdminController@approve')->name('user.approve');
Route::get('/admin/surveys', 'AdminController@surveys')->name('admin.surveys')->middleware('admin');
