<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
	function user(){
		return $this->belongsTo('App\User');
	}

	function questions()
	{
		return $this->hasMany('App\Question');
	}
	
	function answers()
    {
    	return $this->hasMany('App\Answer');
    }
    function criterias()
    {
    	return $this->hasMany('App\Criteria');
    }
    function stats()
    {
    	return $this->hasOne('App\Stat');
    }
    function location()
    {
        return $this->hasOne('App\Location', 'survey_id', 'id');
    }
}
