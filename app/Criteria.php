<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    public function survey()
    {
    	return $this->belongsTo('App\Survey');
    }

    public function existingValues()
    {
    	return $this->hasMany('App\ExistingValue');
    }

    public function proposedValues()
    {
    	return $this->hasMany('App\ProposedValue');
    }
    public function totalValue()
    {
        return $this->hasOne('App\TotalValue');
    }
    function selected()
    {
        return $this->hasOne('App\CriteriaSelect');
    }
}
