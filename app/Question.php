<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    function survey()
    {
    	return $this->belongsTo('App\Survey');
    }

    function choices()
    {
    	return $this->hasMany('App\Choice');
    }

    function answers()
    {
    	return $this->hasMany('App\Answer');
    }
}
