<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','isApproved','isAdmin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function surveys()
    {
        return $this->hasMany('App\Survey');
    }
    function latestSurvey()
    {
        return $this->hasMany('App\Survey')->latest()->first();
    }
    function latestSurveys()
    {
        return $this->hasMany('App\Survey')->latest()->get();
    }

    function answers()
    {
        return $this->hasMany('App\Answer');
    }
    function latestAnswer()
    {
        return $this->hasMany('App\Answer')->latest()->first();
    }
    function latestAnswers()
    {
        return $this->hasMany('App\Answer')->distinct()->get(['survey_id'])->sortByDesc('survey_id');
    }
}
