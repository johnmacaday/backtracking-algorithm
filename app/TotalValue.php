<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TotalValue extends Model
{
	public function criteria()
	{
		return $this->belongsTo('App\Criteria');
	}
   
}
