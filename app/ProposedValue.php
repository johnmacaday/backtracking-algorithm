<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposedValue extends Model
{
    public function criteria()
    {
    	return $this->belongsTo('App\Criteria');
    }
}
