<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    function survey()
    {
    	return $this->belongsTo('App\Survey');
    }
    
    protected $fillable = ['location'];

    protected $casts = [
	    'location' => 'array', // Will convarted to (Array)
	];

}
