<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    function user()
    {
    	return $this->belongsTo('App\User');
    }

    function survey()
    {
    	return $this->belongsTo('App\Survey');
    }

    function question()
    {
    	return $this->belongsTo('App\Question');
    }

    function choice()
    {
    	return $this->belongsTo('App\Choice');
    }
}
