<?php

namespace App\Http\Middleware;

use Closure;

class isApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (auth()->user() &&  auth()->user()->isApproved == 1) {
                return $next($request);
         }
         elseif(auth()->user() &&  auth()->user()->isApproved == 0){
            return redirect()->route('approval');
         }
         else{
            return redirect()->route('login');
         }

    }
}
