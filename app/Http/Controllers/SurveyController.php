<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Survey;
use App\Question;
use App\Choice;
use App\Location;
use App\Graph;
use App\Criteria;

class SurveyController extends Controller
{

	private $matrix = array();
	private $color = array();
	private $cityList = array('ph-7002', 'ph-7006', 'ph-7004', 'ph-7003', 'ph-7001', 'ph-7007', 'ph-7000', 'ph-7008', 'ph-1852', 'ph-7009', 'ph-7010', 'ph-7016', 'ph-7011', 'ph-7012', 'ph-7015', 'ph-7013', 'ph-7014');
	private $cityArray = array('Caloocan', 'Quezon City', 'Valenzuela', 'Malabon', 'Navotas', 'Marikina', 'Manila', 'San Juan', 'Mandaluyong', 'Pasig', 'Makati', 'Pateros', 'Pasig', 'Paranaque', 'Taguig', 'Las Pinas', 'Muntinlupa');
	private $colors = array('#ffb3ba', '#ffffba', '#baffc9', '#bae1ff');

	public function index()
	{
		$surveys = Survey::all();
		return view('survey.index', compact('surveys'));
	}

    public function showQuestion()
    {
    	return view('survey.create');
    }

	public function createSurvey(Request $request)
	{
		// dd($request['criteria']);

		$survey = new Survey();
		$survey->user_id = auth()->id();
		$survey->title = $request->title;
		$survey->save();

		$location = new Location();
		$location->survey_id = $survey->id;
		$a = array_fill(0, 17, 0);
		$b = serialize($a);
		$location->location = $b;
		$location->save();

		//  ---NEW SAVE CODE---
		foreach ($request['qID'] as $qID) {
			$question = new Question();
			$question->survey_id = $survey->id;
			$question->question = $request['question'][$qID];
			$question->save();

			foreach($request['choice'][$qID] as $choices){
				$choice = new Choice();
				$choice->question_id = $question->id;
				$choice->choice = $choices;
				$choice->save();
			}
		}

		foreach($request['criteria'] as $criteria)
    	{
    		$criterias = new Criteria();
    		$criterias->survey_id = $survey->id;
    		$criterias->criteria = $criteria;
    		$criterias->save();
    	}

		return redirect()->back();
	}

	public function show(Survey $survey, Question $questions)
	{
		return view('survey.show', compact('survey', 'questions'));
	}

	public function answerSurvey(Survey $survey)
	{
		$location = unserialize($survey->location->location);
		$cities = $this->cityArray;
		return view('survey.answer', compact('survey','location','cities'));
	}

	public function showCharts(Survey $survey)
	{
		return view('charts.show', compact('survey'));
	}


	public function isSafe($v, $matrix, $color, $c)
    {
        for ($i = 0; ($i < 17); $i++){

            if ($matrix[$v][$i] == 1 && $c == $color[$i]) {
                return false;
            }
        }
        return true;
    }
	public function setVertex($row,$column)
	{
        if($row > 16 || $column > 16) { return false; }
        $this->matrix[$row][$column] = $this->matrix[$column][$row] = 1;
        return true;
    }
    public function graphColoringUtil($graph, $m, $color, $v)
    {
        /* base case: If all vertices are assigned
           a color then return true */
        if ($v == 17)
            return true;
        /* Consider this vertex v and try different colors */
        for ($c = 1; $c <= $m; $c++)
        {
            /* Check if assignment of color c to v s fine*/
            if ($this->isSafe($v, $graph, $color, $c))
            {
            	$this->color[$v] = $c;
            	$color[$v] = $c;
                /* recur to assign colors to rest of the vertices */
                if ($this->graphColoringUtil($graph, $m, $color, $v + 1)) {
                  // echo $v;
                	// $this->color = $color;
                  return true;
                }
                /* If assigning color c doesn't lead to a solution then remove it */
                $this->color[$v] = 0;
                $color[$v] = 0;
            }
        }
        /* If no color can be assigned to this vertex then return false */
        return false;
    }
    public function graphColoring($graph, $m)
    {
        // Initialize all color values as 0. This
        // initialization is needed correct functioning
        // of isSafe()
        $color = array_fill(0, 17, 0);
        // Call graphColoringUtil() for vertex 0
        if (!$this->graphColoringUtil($graph, $m, $color, 0))
        {
            return false;
        }
        // Print the solution
        return $this->color;
    }

    public function createVertices($cityVertex)
    {
    	$this->setVertex($cityVertex[0],$cityVertex[1]); $this->setVertex($cityVertex[0],$cityVertex[2]); $this->setVertex($cityVertex[0],$cityVertex[3]); $this->setVertex($cityVertex[0],$cityVertex[4]); $this->setVertex($cityVertex[0],$cityVertex[6]);
			$this->setVertex($cityVertex[1],$cityVertex[2]); $this->setVertex($cityVertex[1],$cityVertex[5]); $this->setVertex($cityVertex[1],$cityVertex[6]); $this->setVertex($cityVertex[1],$cityVertex[7]); $this->setVertex($cityVertex[1],$cityVertex[8]); $this->setVertex($cityVertex[1],$cityVertex[9]);
			$this->setVertex($cityVertex[2],$cityVertex[3]);
			$this->setVertex($cityVertex[3],$cityVertex[4]);
			$this->setVertex($cityVertex[4],$cityVertex[6]);
			$this->setVertex($cityVertex[5],$cityVertex[9]);
			$this->setVertex($cityVertex[6],$cityVertex[7]); $this->setVertex($cityVertex[6],$cityVertex[8]); $this->setVertex($cityVertex[6],$cityVertex[10]); $this->setVertex($cityVertex[6],$cityVertex[12]);
			$this->setVertex($cityVertex[7],$cityVertex[8]);
			$this->setVertex($cityVertex[8],$cityVertex[9]); $this->setVertex($cityVertex[8],$cityVertex[10]);
			$this->setVertex($cityVertex[9],$cityVertex[10]); $this->setVertex($cityVertex[9],$cityVertex[11]); $this->setVertex($cityVertex[9],$cityVertex[14]);
			$this->setVertex($cityVertex[10],$cityVertex[11]); $this->setVertex($cityVertex[10],$cityVertex[12]); $this->setVertex($cityVertex[10],$cityVertex[14]);
			$this->setVertex($cityVertex[11],$cityVertex[14]);
			$this->setVertex($cityVertex[12],$cityVertex[13]); $this->setVertex($cityVertex[12],$cityVertex[14]);
			$this->setVertex($cityVertex[13],$cityVertex[14]); $this->setVertex($cityVertex[13],$cityVertex[15]); $this->setVertex($cityVertex[13],$cityVertex[16]);
			$this->setVertex($cityVertex[14],$cityVertex[16]);
			$this->setVertex($cityVertex[15],$cityVertex[16]);
    }

	public function showMap(Survey $survey, Location $location)
	{
		$this->color = array();
		$this->matrix = array();
		$cityList = $this->cityList;
		$loca = $survey->location->location;

		$loca = unserialize($loca);
		$matrixSize = 17;

		$adjacencyMatrix = array();
		$cityVertex = array();
		$this->matrix = array_fill(0, $matrixSize, array_fill(0, $matrixSize, 0));

		for ($i=0 ; $i < 17; $i++) {
			if($loca[$i] > 0 ){
				$cityVertex[$i] = $i;
			}
			else{
				$cityVertex[$i] = 17;
			}
		}

		$this->createVertices($cityVertex);
		$adjacencyMatrix = $this->matrix;
		$this->graphColoring($adjacencyMatrix, 4);
		$a = $this->color;
		$colors = $this->colors;
		return view('maps.show', compact('survey','loca', 'a', 'cityVertex', 'cityList', 'colors'));
	}
}
