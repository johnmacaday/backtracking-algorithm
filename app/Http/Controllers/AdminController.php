<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Survey;
use App\Question;
use App\Choice;
use App\Location;
use App\Graph;
use App\Criteria;
use App\User;

class AdminController extends Controller
{
    public function dashboard()
    {
    	$users = User::all();
    	$unApproved = $users->where('isApproved', '=', 0);
    	$approved = $users->where('isApproved', '=', 1);
    	// dd($approved);
        // dd(auth()->user()->isAdmin);
        return view('admin.dashboard', compact('users','unApproved','approved'));
    }

    public function approve(Request $request)
    {
    	// dd($request->approveID);
    	$user = User::find($request->approveID);
    	$user->update(['isApproved' => true]);

    	return redirect()->back();
    }

    public function surveys()
    {
      $surveys = Survey::all();

      return view('admin.surveys', compact('surveys'));
    }
}
