<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria;
use App\Survey;
use App\ExistingValue;
use App\ProposedValue;
use App\TotalValue;
use App\Stat;

class CriteriaController extends Controller
{
    public function show(Survey $survey)
    {
        // $crit = Criteria::findOrFail(1);
        // dd( $crit->survey_id );
    	// dd($survey->answers()->distinct('user_id')->count('user_id'));
    	// dd($survey->criterias->find(1)->existingValues->last()->created_at);

    	return view('values.show', compact('survey'));
    }
}
