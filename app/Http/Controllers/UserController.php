<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Survey;
use App\Question;
use App\Choice;
use App\Location;
use App\Graph;
use App\Criteria;
use App\User;

class UserController extends Controller
{
    public function dashboard(User $user, Survey $survey)
    {
    	$latestBool = false;
    	$answeredBool = false;
    	$user = User::with(['surveys', 'answers', 'surveys.questions','surveys.answers','surveys.criterias','surveys.stats',
    		'surveys.location','surveys.criterias','surveys.questions.choices','surveys.questions.answers'])->find($user->id);
    	if(count($user->surveys()->get()) > 0)
    	{
	    	$latest = $user->latestSurvey();
            $ppl = $latest->answers()->distinct('user_id')->count('user_id');
	    	$latestBool = true;
    	}
    	if($user->answers()->distinct('survey_id')->count('survey_id') > 0)
    	{
    		$latestAnswer = $user->latestAnswer();
    		$answered = $latestAnswer->survey()->first();
    		$answeredBool = true;
    	}
        

        return view('user.dashboard',compact('user','latest','answered','latestBool','answeredBool','ppl','latestAnswer'));
    }

    public function showSurveys(User $user, Survey $survey)
    {
        $surveyBool = false;
        $user = User::with(['surveys', 'answers', 'surveys.questions','surveys.answers','surveys.criterias','surveys.stats',
        'surveys.location','surveys.criterias','surveys.questions.choices','surveys.questions.answers'])->find(auth()->id());
        if (count($user->surveys) > 0) {
            $surveyBool = true;
            $latestSurveys = $user->latestSurveys();}
        return view('survey.show', compact('user','survey','surveyBool','latestSurveys'));
    }

    public function showAnswers(User $user, Survey $survey)
    {
        $answeredBool = false;
        $user = User::with(['surveys', 'answers', 'surveys.questions','surveys.answers','surveys.criterias','surveys.stats',
        'surveys.location','surveys.criterias','surveys.questions.choices','surveys.questions.answers'])->find(auth()->id());
        $survey = Survey::with(['answers','user','questions','user.answers']);
        if($user->answers()->distinct('survey_id')->count('survey_id') > 0)
        {
            $answers = $user->latestAnswers();
            $answeredBool = true;
        }
        return view('user.answers',compact('survey','user','answers','answeredBool'));
    }
}
