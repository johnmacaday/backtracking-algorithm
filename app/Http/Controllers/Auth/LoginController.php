<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
use AuthenticatesUsers;
        protected function authenticated(Request $request, $user)
        {
            if(auth()->user()->isAdmin){
                return redirect()->route('admin.dashboard');
            }
            elseif(auth()->user()->isApproved){
                return redirect()->route('dashboard',$user->id);
            }
            else{
                return redirect()->route('approval');
            }
            $this->middleware('guest')->except('logout');
        }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $users = User::all();
         $count = count($users->where('isAdmin', '=', 1));
         if($count < 1)
         {
            User::create([
                'name' => 'admin',
                'email' => 'admin@admin.com',
                'isApproved' => true,
                'isAdmin' => true,
                'password' => Hash::make('admin123'),
             ]);
            auth()->logout();
         }

        $this->middleware('guest')->except('logout');
    }
}
