<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Survey;
use App\Answer;
use App\User;
use App\Stat;
use App\ExistingValue;
use App\ProposedValue;
use App\TotalValue;
use App\Question;
use App\Choice;
use App\Location;
use App\Graph;
use App\Criteria;

class AnswerController extends Controller
{
	private $matrix = array();
	private $color = array();
	private $cityList = array('ph-7002', 'ph-7006', 'ph-7004', 'ph-7003', 'ph-7001', 'ph-7007', 'ph-7000', 'ph-7008', 'ph-1852', 'ph-7009', 'ph-7010', 'ph-7016', 'ph-7011', 'ph-7012', 'ph-7015', 'ph-7013', 'ph-7014');
	private $cityArray = array('Caloocan', 'Quezon City', 'Valenzuela', 'Malabon', 'Navotas', 'Marikina', 'Manila', 'San Juan', 'Mandaluyong', 'Pasig', 'Makati', 'Pateros', 'Pasig', 'Paranaque', 'Taguig', 'Las Pinas', 'Muntinlupa');
	private $colors = array('#ffb3ba', '#ffffba', '#baffc9', '#bae1ff');

    public function save(Request $request)
	{
		for ($i=0; $i < count($request['question']); $i++)
		{

				$answer = new Answer();
				$answer->user_id = auth()->id();
				$answer->survey_id = $request->survey;
				$answer->question_id = $request['question'][$i];
				$answer->choice_id = $request['answer'][$i];
				$answer->save();

		}

		for ($i=0; $i < count($request['criteria']); $i++)
    	{
    		$existing = new ExistingValue();
    		$proposed = new ProposedValue();

    		$existing->criteria_id = $request['criteria'][$i];
    		$proposed->criteria_id = $request['criteria'][$i];
    		$existing->value = $request['existingValue'][$i];
    		$proposed->value = $request['proposedValue'][$i];
    		$existing->save();
    		$proposed->save();
    	}

    	$survey =  Survey::findOrFail($request->survey);
    	$location = $survey->location;
    	$loca = unserialize($location->location);
    	$loca[$request->location] += 1;
    	$serialLoca = serialize($loca);
    	$location->update(['location' => $serialLoca]);

    	$location = unserialize($survey->location->location);
    	$location[$request->location] += 1;

    	$esTotal = 0.00;
    	$psTotal = 0.00;
    	$ppl = $survey->answers()->distinct('user_id')->count('user_id');
    	$count = count($survey->criterias);

    	foreach ($survey->criterias as $criteria) {

    		$j = $criteria->id;
    		for ($i=1; $i <= 5; $i++) {
    			$ex = count($criteria->existingValues->where('value', '==', $i));

	    		if($ex > 0){
	    			$eVal = ($i * $ex) / $ppl;
	    			$eTotal[$j][$i] = $eVal;
		    		$esTotal += $eVal;
	    		}
	    		else{
	    			$eTotal[$j][$i] = 0;
	    		}
    		}
    		for ($i=1; $i <= 5; $i++) {
    			$px = count($criteria->proposedValues->where('value', '==', $i));

	    		if($px > 0){
	    			$pVal = ($i * $px) / $ppl;
	    			$pTotal[$j][$i] = $pVal;
		    		$psTotal += $pVal;
	    		}
	    		else{
	    			$pTotal[$j][$i] = 0;
	    		}
    		}
    	}
    	$esMean = $esTotal / $count;
    	$psMean = $psTotal / $count;

    	foreach ($survey->criterias as $criteria) {
    		$j = $criteria->id;
    		$pssTotal[$j] = array_filter($pTotal[$j]);
    		$pValTotal[$j] = array_sum($pssTotal[$j]);
    		$essTotal[$j] = array_filter($eTotal[$j]);
    		$eValTotal[$j] = array_sum($essTotal[$j]);
    		if(!$criteria->totalvalue()->exists())
    		{
    			$totalvalues = new TotalValue();
				$totalvalues->criteria_id = $criteria->id;
				$totalvalues->existingTotal = $eValTotal[$j] ;
				$totalvalues->proposedTotal = $pValTotal[$j] ;
				$totalvalues->save();
    		}	else{
    			$totalvalues = $criteria->totalValue;
    			$totalvalues->existingTotal = $eValTotal[$j] ;
				$totalvalues->proposedTotal = $pValTotal[$j] ;
				$totalvalues->save();
    		}
    	}

    	$xeStd = 0;
        $xpStd = 0;
    	foreach ($survey->criterias as $criteria)
    	{
    		$exStd = $eValTotal[$criteria->id] + $esMean;
    		$exStd = pow($exStd, 2);
    		$xeStd += $exStd;
    		$pxStd = $pValTotal[$criteria->id] + $psMean;
    		$pxStd = pow($pxStd, 2);
    		$xpStd += $pxStd;
    	}
    	$cat = count($survey->criterias) - 1;
    	$std = $xeStd / $cat;
    	$eStd = sqrt($std);
    	$std = $xpStd / $cat;
    	$pStd = sqrt($std);

    	$a = $psMean - $esMean;
    	$x = pow($eStd, 2) / $count;
    	$y = pow($pStd, 2) / $count;
    	$b = $x + $y;
    	$tTest = $a / sqrt($b);
    	if (!$survey->stats()->exists()) {
    		$stats = new Stat();
    		$stats->survey_id = $survey->id;
    		$stats->esMean = $esMean;
    		$stats->psMean = $psMean;
    		$stats->eStd = $eStd;
    		$stats->pStd = $pStd;
    		$stats->tTest = $tTest;
    		$stats->save();
    	}
        else{
    		$stats = $survey->stats;
    		$stats->esMean = $esMean;
    		$stats->psMean = $psMean;
    		$stats->eStd = $eStd;
    		$stats->pStd = $pStd;
    		$stats->tTest = $tTest;
    		$stats->save();
    	}

		return redirect()->route('dashboard',auth()->id());

	}

	public function isSafe($v, $matrix, $color, $c)
    {
        for ($i = 0; ($i < 17); $i++){

            if ($matrix[$v][$i] == 1 && $c == $color[$i]) {
                return false;
            }
        }
        return true;
    }
	public function setVertex($row,$column) {
        if($row > 16 || $column > 16) { return false; }
        $this->matrix[$row][$column] = $this->matrix[$column][$row] = 1;
        return true;
    }
    public function graphColoringUtil($graph, $m, $color, $v)
    {
        /* base case: If all vertices are assigned
           a color then return true */
        if ($v == 17)
            return true;
        /* Consider this vertex v and try different colors */
        for ($c = 1; $c <= $m; $c++)
        {
            /* Check if assignment of color c to v s fine*/
            if ($this->isSafe($v, $graph, $color, $c))
            {
            	$this->color[$v] = $c;
            	$color[$v] = $c;
                /* recur to assign colors to rest of the vertices */
                if ($this->graphColoringUtil($graph, $m, $color, $v + 1)) {
                  // echo $v;
                	// $this->color = $color;
                  return true;
                }
                /* If assigning color c doesn't lead to a solution then remove it */
                $this->color[$v] = 0;
                $color[$v] = 0;
            }
        }
        /* If no color can be assigned to this vertex then return false */
        return false;
    }
    public function graphColoring($graph, $m)
    {
        // Initialize all color values as 0. This
        // initialization is needed correct functioning
        // of isSafe()
        $color = array_fill(0, 17, 0);
        // Call graphColoringUtil() for vertex 0
        if (!$this->graphColoringUtil($graph, $m, $color, 0))
        {
            echo "nope";
            return false;
        }
        // Print the solution
        return $this->color;
    }

    public function createVertices($cityVertex)
    {
    	$this->setVertex($cityVertex[0],$cityVertex[1]); $this->setVertex($cityVertex[0],$cityVertex[2]); $this->setVertex($cityVertex[0],$cityVertex[3]); $this->setVertex($cityVertex[0],$cityVertex[4]); $this->setVertex($cityVertex[0],$cityVertex[6]);
		$this->setVertex($cityVertex[1],$cityVertex[2]); $this->setVertex($cityVertex[1],$cityVertex[5]); $this->setVertex($cityVertex[1],$cityVertex[6]); $this->setVertex($cityVertex[1],$cityVertex[7]); $this->setVertex($cityVertex[1],$cityVertex[8]); $this->setVertex($cityVertex[1],$cityVertex[9]);
		$this->setVertex($cityVertex[2],$cityVertex[3]);
		$this->setVertex($cityVertex[3],$cityVertex[4]);
		$this->setVertex($cityVertex[4],$cityVertex[6]);
		$this->setVertex($cityVertex[5],$cityVertex[9]);
		$this->setVertex($cityVertex[6],$cityVertex[7]); $this->setVertex($cityVertex[6],$cityVertex[8]); $this->setVertex($cityVertex[6],$cityVertex[10]); $this->setVertex($cityVertex[6],$cityVertex[12]);
		$this->setVertex($cityVertex[7],$cityVertex[8]);
		$this->setVertex($cityVertex[8],$cityVertex[9]); $this->setVertex($cityVertex[8],$cityVertex[10]);
		$this->setVertex($cityVertex[9],$cityVertex[10]); $this->setVertex($cityVertex[9],$cityVertex[11]); $this->setVertex($cityVertex[9],$cityVertex[14]);
		$this->setVertex($cityVertex[10],$cityVertex[11]); $this->setVertex($cityVertex[10],$cityVertex[12]); $this->setVertex($cityVertex[10],$cityVertex[14]);
		$this->setVertex($cityVertex[11],$cityVertex[14]);
		$this->setVertex($cityVertex[12],$cityVertex[13]); $this->setVertex($cityVertex[12],$cityVertex[14]);
		$this->setVertex($cityVertex[13],$cityVertex[14]); $this->setVertex($cityVertex[13],$cityVertex[15]); $this->setVertex($cityVertex[13],$cityVertex[16]);
		$this->setVertex($cityVertex[14],$cityVertex[16]);
		$this->setVertex($cityVertex[15],$cityVertex[16]);
    }

	public function show(Survey $survey, Location $location)
	{

		$survey = Survey::with(['questions', 'answers', 'criterias', 'stats', 'location', 'questions.choices', 'questions.answers', 'criterias.existingValues', 'criterias.proposedValues'])->find($survey->id);
		$surveyBool = false;
		if ($ppl = $survey->answers()->distinct('user_id')->count('user_id') > 0) {
		$this->color = array();
		$this->matrix = array();
		$cityList = $this->cityList;
		$loca = $survey->location->location;
		$loca = unserialize($loca);
    $ppl = $survey->answers()->distinct('user_id')->count('user_id');
		$matrixSize = 17;

		$adjacencyMatrix = array();
		$cityVertex = array();
		$this->matrix = array_fill(0, $matrixSize, array_fill(0, $matrixSize, 0));

		for ($i=0 ; $i < 17; $i++) {
			if($loca[$i] > 0 ){
				$cityVertex[$i] = $i;
			}
			else{
				$cityVertex[$i] = 17;
			}
		}
        $surveyBool = false;
		$this->createVertices($cityVertex);
		$adjacencyMatrix = $this->matrix;
		$this->graphColoring($adjacencyMatrix, 4);
		$a = $this->color;
		$colors = $this->colors;
        if ($survey->criterias[0]->totalValue != null) {
            $surveyBool = true;
        }
        if(!$surveyBool){
            return redirect()->route('dashboard', auth()->id());
        }
		return view('answer.show', compact('survey','loca', 'a', 'cityVertex', 'cityList', 'colors','surveyBool','ppl'));
		}
		else{
			return view('answer.show', compact('surveyBool'));
		}
	}

    public function joinSurvey()
    {
        return view('answer.join');
    }

    public function surveyJoin(Request $request)
    {
        $user = User::find(auth()->id());
				if (Survey::where('id', '=', $request->link_id)->exists()) {
        	$linkedSurvey = Survey::find($request->link_id);
	        if($linkedSurvey->user_id == $user->id){
	            return redirect()->back()->with('errors', "You can't answer your own survey");
	        }
	        // dd($user->answers());
	        $userAnswered = $user->answers()->where('survey_id', '=', $request->link_id)->count();
	        // dd($userAnswered);
	        if($userAnswered > 0){
	            return redirect()->back()->with('errors', 'You have already answered that survey');
	        }
            return redirect()->route('survey.answer', [$request->link_id]);
        }
        else{
            return redirect()->back()->with('errors', 'That ID does not exist');
        }
    }

}
