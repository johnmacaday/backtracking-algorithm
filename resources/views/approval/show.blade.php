@extends('layouts.app')

@section('head-scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var left = document.getElementById("dash-main");
			var height = window.innerHeight;
			height -= 135;
			if(height > 1080){
			left.style.height = 1080 + "px";
			} else{
			left.style.height = height + "px";
			}
		});
	</script>

@endsection

@section('content')
	
		<div class="row">
					<div class="container">
						<h2 class="mt-3">Submission Successful</h2>
						<div class="card alert alert-info mt-5 p-4">
							Your Application Has Been Sent for Approval
						</div>
					</div>
				</div>
@endsection