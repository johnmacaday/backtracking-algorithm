@extends('layouts.app')

@section('content')
	<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
		<div class="container">
		@if ($answeredBool)
			<div class="row">
			{{-- expr --}}
			@foreach ($answers as $answer)
			<hidden>{{$survey = $survey->find($answer->survey_id)}}</hidden>
				{{-- expr --}}
				{{-- {{dd($survey->find($answer->survey_id)->answers()->where('user_id', '=', $user->id)->latest()->first()->created_at)}} --}}
				<div class="card p-3 mx-auto mb-5 col-sm-5">
				<div>
					{{-- {{dd($survey->find($answer->survey_id))}} --}}
					<h3 style="border-bottom: 1px solid lightgray;">{{$survey->title}}</h3>
					<h5>Created By: {{$user->find($survey->user_id)->name}}</h5>
					<h5>Date Created: {{$answer->survey()->first()->created_at->toFormattedDateString()}}</h5>
{{-- 					{{dd($user->answers()->where('survey_id', '=', $answer->id)->latest()->first())}} --}}
					<h5>Date Answered: {{$survey->find($answer->survey_id)->answers()->where('user_id', '=', $user->id)->latest()->first()->created_at->toFormattedDateString()}}</h5>
					{{-- {{dd($answer->survey()->get())}} --}}
				</div>
			</div>
		</div>
			@endforeach
		@else
			<div class="card mb-5 p-4">
				<div class="card p-3 mb-3 text-center">
					<h4>You have not answered any survey</h4>
				</div>
			</div>
		@endif
	</div>
	</div>
</main>
</div>
@endsection
