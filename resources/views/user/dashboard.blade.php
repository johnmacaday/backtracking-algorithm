@extends('layouts.app')

@section('head-scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var left = document.getElementById("dash-main");
			var height = window.innerHeight;
			height -= 135;
			if(height > 1080){
			left.style.height = 1080 + "px";
			} else{
			left.style.height = height + "px";
			}
		});
	</script>

@endsection

@section('content')

		<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
					<div class="container">
						<h2 class="mt-3">Recently Created Survey</h2>
						<div class="card mb-5 p-4">
							@if ($latestBool)
										<div class="card p-3 mb-3">
											<h3 class="mb-4">{{$latest->title}}</h3>
											<h5>Date Created: {{$latest->created_at->toFormattedDateString()}}</h5>
											<h5>Total Respondents: {{$ppl}}</h5>
										</div>
									<a class="btn btn-primary m-auto" style="width: 30%" 	href="{{url('answers/'.$latest->id)}}">View</a>
							@else
								<div class="card p-3 mb-3 text-center">
									<h4>You have not created any survey</h4>
								</div>
							@endif

						</div>
					</div>
					<div class="container">
						<h2>Recently Answered Survey</h2>
						<div class="card mb-3 p-4">
							@if ($answeredBool)
								<h3>{{$answered->title}}</h3>
								<h5>Created by: {{$answered->user()->first()->name}}</h5>
								<h5>Date Created: {{$answered->created_at->toFormattedDateString()}}</h5>
								<h5>Date Answered: {{$user->answers()->where('survey_id', '=', $answered->id)->latest()->first()->created_at->toFormattedDateString()}}</h5>
								@else
								<div class="card p-3 mb-3 text-center">
									<h4>You have not answered any survey</h4>
								</div>
							@endif
						</div>
					</div>
				</div>
			</main>
		</div>
@endsection
