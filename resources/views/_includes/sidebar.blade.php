<nav class="col-sm-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      @if (auth()->user()->isAdmin)
        <li class="nav-item">
          <a class="nav-link @if (Route::current()->getName() == 'admin.dashboard') active @endif" href="{{route('admin.dashboard')}}">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>@if (Route::current()->getName() == 'admin.dashboard') <span class="sr-only">(current)</span>@endif
            Admin Dashboard
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link @if (Route::current()->getName() == 'admin.surveys') active @endif" href="{{ route('admin.surveys')}}">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>@if (Route::current()->getName() == 'survey.show') <span class="sr-only">(current)</span>@endif
            All Surveys
          </a>
        </li>
      @endif

      @if (auth()->user()->isApproved && !auth()->user()->isAdmin)
      <li class="nav-item">
        <a class="nav-link @if (Route::current()->getName() == 'dashboard') active @endif" href="{{ url('dashboard/'.auth()->id()) }}">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>@if (Route::current()->getName() == 'dashboard') <span class="sr-only">(current)</span>@endif
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link @if (Route::current()->getName() == 'survey.create')active @endif" href="{{ url('survey/create') }}">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>@if (Route::current()->getName() == 'survey.create') <span class="sr-only">(current)</span>@endif
          Create a Survey
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link @if (Route::current()->getName() == 'survey.join')active @endif" href="{{ url('join-survey') }}">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
          Answer a Survey
        </a>
      </li>
    </ul>

    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
      <span>Saved Surveys</span>
    </h6>
    <ul class="nav flex-column mb-2">
      <li class="nav-item">
        <a class="nav-link @if (Route::current()->getName() == 'survey.show') active @endif" href="{{ url('my-surveys/'.auth()->id()) }}">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>@if (Route::current()->getName() == 'survey.show') <span class="sr-only">(current)</span>@endif
          My Surveys
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link @if (Route::current()->getName() == 'answers.show') active @endif" href="{{ url('my-answers/'.auth()->id()) }}">
          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>@if (Route::current()->getName() == 'answers.show') <span class="sr-only">(current)</span>@endif
          Answered Surveys
        </a>
      </li>
    </ul>
    @endif
  </div>
</nav>
