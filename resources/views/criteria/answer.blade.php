@extends('layouts.app')	

@section('style')

	<style>
		
		td, tr, th{
			border: 1px solid black;
		}

	</style>

@endsection

@section('content')
	<div class="container">
	<h1> {{$survey->title}} </h1>
		<form method="POST" action="/criteria/values">
			@csrf
		<table style="width:100%">
		  <caption></caption>
		  <tr>
		    <th rowspan="2" >Criteria</th>
		    <th colspan="5">Existing System</th>
		    <th colspan="5">Proposed System</th>
		  </tr>
		  <tr>
		    <td>5</td>
		    <td>4</td>
		    <td>3</td>
		    <td>2</td>
		    <td>1</td>
		    <td>5</td>
		    <td>4</td>
		    <td>3</td>
		    <td>2</td>
		    <td>1</td>
		  </tr>
		  <hidden>{{ $q = 0 }}</hidden>
		  @foreach ($survey->criterias as $criteria)
		  <tr>
		  	<input type="hidden" name="criteria[]" value={{$criteria->id}}>
		    <td>{{ $criteria->criteria }}</td>
		    <td><input type="radio" name="existingValue[{{number_format($q)}}]" value="5"></td>
		    <td><input type="radio" name="existingValue[{{number_format($q)}}]" value="4"></td>
		    <td><input type="radio" name="existingValue[{{number_format($q)}}]" value="3"></td>
		    <td><input type="radio" name="existingValue[{{number_format($q)}}]" value="2"></td>
		    <td><input type="radio" name="existingValue[{{number_format($q)}}]" value="1"></td>
		    <td><input type="radio" name="proposedValue[{{number_format($q)}}]" value="5"></td>
		    <td><input type="radio" name="proposedValue[{{number_format($q)}}]" value="4"></td>
		    <td><input type="radio" name="proposedValue[{{number_format($q)}}]" value="3"></td>
		    <td><input type="radio" name="proposedValue[{{number_format($q)}}]" value="2"></td>
		    <td><input type="radio" name="proposedValue[{{number_format($q)}}]" value="1"></td>
		  </tr>
		  <hidden>{{ $q++ }}</hidden>
		  @endforeach
		</table>
		<input type="submit" value="Submit">
	</form>
	</div>



@endsection