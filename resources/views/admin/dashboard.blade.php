@extends('layouts.app')

@section('head-scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var left = document.getElementById("dash-main");
			var height = window.innerHeight;
			height -= 135;
			if(height > 1080){
			left.style.height = 1080 + "px";
			} else{
			left.style.height = height + "px";
			}
		});
	</script>

@endsection

@section('content')

		<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
					<div class="container">
						<h2 class="mt-3 mb-5 text-center">Registered Users</h2>
						<ul class="nav nav-tabs mb-5" id="myTab" role="tablist">
						  <li class="nav-item text-center col-sm-6">
						    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Pending Approval<span  style="vertical-align: top" class="ml-3 my-auto badge badge-info">{{count($unApproved)}}</span></a>
						  </li>
						  <li class="nav-item text-center col-sm-6">
						    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Approved Users</a>
						  </li>
						</ul>
						<div class="tab-content" id="myTabContent">
		 					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
		 						@if (count($unApproved) > 0)
		 							{{-- expr --}}
								@foreach ($unApproved as $notApproved)
									<form action="{{ route('user.approve') }}" method="POST">
										@csrf
									<div class="card mb-3 p-4 col-sm-6 mx-auto">
										<input type="hidden" name="approveID" value="{!! $notApproved->id !!}">
										<h4>{{$notApproved->name}}</h4>
										<h5>Email: {{$notApproved->email}}</h5>
										<p><span class="font-weight-bold">Request Date: </span>{{$notApproved->created_at->toFormattedDateString() }}</p>
										<button type="submit" class="btn btn-primary">Approve Application</button>
									</div>
									</form>
								@endforeach

								@else
									<div class="card mb-3 p-4 col-sm-6 mx-auto text-center">
										<h3>No Pending Applications</h3>
									</div>
								@endif
		 					</div>

		 					<div class="tab-pane fade show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
		 						@foreach ($approved as $user)
		 						@if ($user->isAdmin)
		 							@continue
		 						@endif
		 							<div class="card mb-3 p-4 col-sm-6 mx-auto">
										<h4>{{$user->name}}</h4>
										<h5>Email: {{$user->email}}</h5>
										<p><span class="font-weight-bold">Approve Date: </span>{{$user->updated_at->toFormattedDateString() }}</p>
									</div>
		 						@endforeach
		 					</div>
		 				</div>
					</div>
				</div>
			</main>
		</div>
@endsection
