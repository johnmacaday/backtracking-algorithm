@extends('layouts.app')

@section('head-scripts')
	<script type="text/javascript">
		$(document).ready(function(){
			var left = document.getElementById("dash-main");
			var height = window.innerHeight;
			height -= 135;
			if(height > 1080){
			left.style.height = 1080 + "px";
			} else{
			left.style.height = height + "px";
			}
		});
	</script>

@endsection

@section('content')

		<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
					<div class="container">
            <div class="card mb-5 p-4">
              @foreach ($surveys as $survey)
                <div class="card p-3 mb-3">
                  <h3 class="mb-4">{{$survey->title}}</h3>
									<h5>Survey ID: {{$survey->id}}</h5>
                  <h5>Date Created: {{$survey->created_at->toFormattedDateString()}}</h5>
                  <h5>Created By: {{$survey->user()->first()->name}}</h5>
                  <h5>Email: {{$survey->user()->first()->email}}</h5>
                  <h5>Total Respondents: {{$survey->answers()->distinct('user_id')->count('user_id')}}</h5>
                </div>
                <a class="btn btn-primary m-auto" style="width: 30%" 	href="{{url('answers/'.$survey->id)}}">View</a>
              @endforeach
						</div>
          </div>
				</div>
			</main>
		</div>
@endsection
