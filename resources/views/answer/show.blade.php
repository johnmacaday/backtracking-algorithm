@extends('layouts.app')

@section('head-scripts')
	<script src="{{asset('js/Chart.min.js')}}"></script>
	<script src="{{asset('js/highmaps.js')}}"></script>
	<script src="{{asset('js/exporting.js')}}"></script>
	<script src="{{asset('js/ph-all.js')}}"></script>
@endsection

@section('style')
<link rel="stylesheet" href="{{asset('css/maps.css')}}">
	<style>
		.nav-tabs {
	    	border-bottom: 1px solid #dee2e6 !important;
		}
		thead th {
		  border-top: 1px solid #dddddd;
		  border-bottom: 1px solid #dddddd;
		  border-right: 1px solid #dddddd;
		}
	</style>
@endsection

@section('content')
	@if ($surveyBool)
		{{-- expr --}}

	<div class="container">
		<ul class="nav nav-tabs mb-5" id="myTab" role="tablist" style="font-size: 18px;">
		  <li class="nav-item">
		    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Survey Answers</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Survey Stats</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Heatmap</a>
		  </li>
		</ul>
		<div class="tab-content" id="myTabContent">
		  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
		  	<h1>{{$survey->title}}</h1>
		  	<h5 class="mt-3">Tally of answers</h5>
			@foreach ($survey->questions as $question)
				<div class="card p-3 mb-5">
					<h3 style="border-bottom: 1px solid lightgray;">{{ $question->question }}</h3>
					<h5>Choices: </h5>
						@foreach ($question->choices as $choice)
							<div class="mt-3" style="font-size: 16px">
								<span class="font-weight-bold" >{{ $choice->choice }}</span>
								<div class="container row">
								<span>Total: {{ $choice->answers->count() }}</span>
								<div class="col-sm-6">
								<div class="progress" style="height: 15px;border-radius:10px; font-size: 12px;">
									<div class="progress-bar" role="progressbar" aria-valuenow="{{number_format(($choice->answers->count() / $ppl) * 100, 2)}}" aria-valuemin="0" aria-valuemax="100" style="width:{!!number_format(($choice->answers->count() / $ppl) * 100, 2)!!}%;">{{number_format(($choice->answers->count() / $ppl) * 100, 2)}}%</div>
								</div>
								</div>
							</div>
							</div>
						@endforeach
				</div>
			@endforeach
				<div class="card mb-5">
					<table style="width:100%" id="statsTable" class="cell-border">
						<thead>
						  <tr>
						    <th>Criteria</th>
						    <th colspan="5">Existing System</th>
						    <th colspan="5">Proposed System</th>
						  </tr>
						</thead>
						<tbody>
							<tr>
						  	<td></td>
						    <th>Very Effective</th>
						    <th>Effective</th>
						    <th>Moderate</th>
						    <th>Slightly Effective</th>
						    <th>Not Effective</th>
								<th>Very Effective</th>
						    <th>Effective</th>
						    <th>Moderate</th>
						    <th>Slightly Effective</th>
						    <th>Not Effective</th>
						  </tr>
							  <tr>
							  	<th></th>
							    @for ($i = 5; $i >0; $i--)
									<th>{{ $i }}</th>
								@endfor
							    @for ($i = 5; $i >0; $i--)
									<th>{{ $i }}</th>
								@endfor
							  </tr>
							  @foreach ($survey->criterias as $criteria)
							  <tr>
								<th>{{ $criteria->criteria }}</th>
								@for ($i = 5; $i >0; $i--)
									<td>{{ count($criteria->existingValues->where('value', '==', $i)) }}</td>
								@endfor
								@for ($i = 5; $i > 0; $i--)
									<td>{{ count($criteria->proposedValues->where('value', '==', $i)) }}</td>
								@endfor
							  </tr>
							  @endforeach
						</tbody>
					</table>
			</div>
		  </div>

		  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

			<div class="container mb-5">
				<canvas id="myChart" width="400" height="200"></canvas>
			</div>

			<div class="container mb-5 tTestChart">
				<caption><h3>T-test of the Existing and the Proposed System</h3></caption>
				<div class="card mb-5">
					<table id="tTestChart1" class="displayTables cell-border" style="width:100%">
					<thead>
						<tr>
							<th>Criteria</th>
							<th>Mean of the Existing System</th>
							<th>Verbal Interpretation</th>
							<th>Mean of the Proposed System</th>
							<th>Verbal Interpretation</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($survey->criterias as $criteria)
						<tr>
							<th>{{$criteria->criteria}}</th>
							<td>{{number_format($criteria->totalValue->existingTotal, 3)}}</td>
							<td>
							@if(number_format($criteria->totalValue->existingTotal, 3) < 1.51)
								{{'Not Effective'}}
							@elseif(number_format($criteria->totalValue->existingTotal, 3) < 2.51)
								{{'Slightly Effective'}}
							@elseif(number_format($criteria->totalValue->existingTotal, 3) < 3.51)
								{{'Moderate'}}
							@elseif(number_format($criteria->totalValue->existingTotal, 3) < 4.51)
								{{'Effective'}}
							@else
								{{'Very Effective'}}
							@endif
							</td>
							<td>{{number_format($criteria->totalValue->proposedTotal, 3)}}</td>
							<td>
							@if(number_format($criteria->totalValue->proposedTotal, 3) < 1.51)
								{{'Not Effective'}}
							@elseif(number_format($criteria->totalValue->proposedTotal, 3) < 2.51)
								{{'Slightly Effective'}}
							@elseif(number_format($criteria->totalValue->proposedTotal, 3) < 3.51)
								{{'Moderate'}}
							@elseif(number_format($criteria->totalValue->proposedTotal, 3) < 4.51)
								{{'Effective'}}
							@else
								{{'Very Effective'}}
							@endif
							</td>
						</tr>
						@endforeach
						<tr>
							<th>Weighted Mean</th>
							<th>{{number_format($survey->stats->esMean, 3)}}</th>
							<th>
								@if(number_format($survey->stats->esMean, 3) < 1.51)
									{{'Not Effective'}}
								@elseif(number_format($survey->stats->esMean, 3) < 2.51)
									{{'Slightly Effective'}}
								@elseif(number_format($survey->stats->esMean, 3) < 3.51)
									{{'Moderate'}}
								@elseif(number_format($survey->stats->esMean, 3) < 4.51)
									{{'Effective'}}
								@else
									{{'Very Effective'}}
								@endif
							</th>
							<th>{{number_format($survey->stats->psMean,3)}}</th>
							<th>
								@if(number_format($survey->stats->psMean,3) < 1.51)
									{{'Not Effective'}}
								@elseif(number_format($survey->stats->psMean,3) < 2.51)
									{{'Slightly Effective'}}
								@elseif(number_format($survey->stats->psMean,3) < 3.51)
									{{'Moderate'}}
								@elseif(number_format($survey->stats->psMean,3) < 4.51)
									{{'Effective'}}
								@else
									{{'Very Effective'}}
								@endif
							</th>
						</tr>
					</tbody>
				</table>
				</div>
			</div>
			<div class="container tTestChart mb-5">
				<caption><h3>T-test of the Existing and the Proposed System</h3></caption>
				<div class="card mb-5">
				<table id="tTestChart2" class="displayTables cell-border" style="width:100%">
					<thead>
					  <tr>
					    <th>System</th>
					    <th>Weighted Mean</th>
					    <th>Standard Deviation</th>
					    <th>Total Number of Criteria</th>
					  </tr>
				 	</thead>
				 	<tbody>
					  <tr>
					    <th>Existing</th>
					    <td>{{number_format($survey->stats->esMean, 3)}}</td>
					    <td>{{number_format($survey->stats->eStd, 3)}}</td>
					    <td>{{count($survey->criterias)}}</td>
					  </tr>
					   <tr>
					    <th>Proposed</th>
					    <td>{{number_format($survey->stats->psMean, 3)}}</td>
					    <td>{{number_format($survey->stats->pStd, 3)}}</td>
					    <td>{{count($survey->criterias)}}</td>
					  </tr>
					   <tr>
					    <th>T-Test Result</th>
					    <th colspan="3">{{number_format($survey->stats->tTest, 3)}}</th>
					  </tr>
					</tbody>

				</table>
			</div>
			</div>
		  </div>

		  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
			<div id="map-container">

			</div>
		  </div>
		</div>

	</div>
@else
<div class="container">
		<div class="card p-3 mb-3 text-center">
			<h4>The survey doesn't have answers yet</h4>
		</div>
</div>
@endif
@endsection

@section('scripts')
@if ($surveyBool)
	<script>

			$('#statsTable').DataTable( {
		        "paging":   false,
		        "ordering": false,
		        "info":     false,
		        "searching": false,
		    });
	</script>
	<script>
			$('table.displayTables').DataTable( {
		        "paging":   false,
		        "ordering": false,
		        "info":     false,
		        "searching": false,
		    });
		</script>
		<script>
			var ctx = document.getElementById("myChart").getContext("2d");
			var myChart = new Chart(ctx, {
			    type: 'bar',
			    data: {
			    datasets: [{
						label: 'Existing System',
						data: [	@foreach ($survey->criterias as $criteria)
										{!!number_format($criteria->totalValue->existingTotal, 2).","!!}
									@endforeach],
				      	backgroundColor: 'rgba(126,206,255, 0.3)',
					    borderColor: 'rgba(126,190,255, 1)',
					    borderWidth: 1
			        },

			         {
			          label: 'Proposed System',
			          data: [@foreach ($survey->criterias as $criteria)
									{!! number_format($criteria->totalValue->proposedTotal, 2).","!!}
								@endforeach],
			          backgroundColor: 'rgba(126,158,255, 0.3)',
					    borderColor: 'rgba(126,142,255,1)',
					    borderWidth: 1

			          // Changes this dataset to become a line
			          // type: 'line'
			        }],
			    labels: [@foreach ($survey->criterias as $criteria)
									{!!"'".$criteria->criteria ."'". ","!!}
								@endforeach]
			  },
			    options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }],
			        },
			        layout: {
			            padding: {
			                left: 50,
			                right: 0,
			                top: 0,
			                bottom: 0
			            }
			        },
			        legend: {
			            labels: {
			                // This more specific font property overrides the global property
			                fontStyle: 'bold',
			            }
			        }
			    }
			});
        	var url_base64jp = document.getElementById("myChart").toDataURL("image/jpg");
        	link1.href=url_base64jp;
			</script>
			<script>
				var data = [
				    @for ($i = 0; $i < 17; $i++)
						{!! "['" . $cityList[$i] ."',"!!}
						@if ($loca[$i] > 0)
							{!!$loca[$i]."," . "'".$colors[$a[$i] - 1]."'],"!!}
						@else
							{!! 0 ."," . "'" . "#ffffff" ."'],"  !!}
						@endif
					@endfor
				];

				// Create the chart
				Highcharts.mapChart('map-container', {
				    chart: {
				        map: 'countries/ph/ph-all',
				        events: {
				        load: function () {
				            this.mapZoom(0.025, 1780, -5650);
				            }
				        }
				    },

				    title: {
				        text: 'Heatmap of Where Respondents Live'
				    },

				    subtitle: {
				        text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/ph/ph-all.js">Philippines</a>'
				    },

				    mapNavigation: {
				        enabled: false,
				        /* buttonOptions: {
				            verticalAlign: 'bottom'
				        } */
				    },

				    series: [{
				        data: data,
				        name: 'Heatmap',
				        keys: ['hc-key','value', 'color'],
				        states: {
				            hover: {
				                color: '#ffdfba',
				            }
				        },
				        dataLabels: {
				            enabled: true,
				            format: '{point.name}',
				            crop: false,
				            overflow: 'none'
				        }
				    }]
				});
		</script>
		@endif
@endsection
