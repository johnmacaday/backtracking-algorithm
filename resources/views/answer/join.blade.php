@extends('layouts.app')

@section('style')

@endsection

@section('content')
	{{-- expr --}}

<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
	<div class="container mb-5">

<form action="{{ route('join.survey') }}" method="POST">
								@csrf
								<div class="card p-5 mt-5">
								<h3 class="text-center mt-5 mb-5">Answer a Survey</h3>
		            <div class="card mb-5">

		                <div class="card-header">

								<div class="col-md-12">
									<div class="row">
						                <label for="link_id" class="col-sm-2 col-form-label text-md-right">Survey ID:</label>

						                <div class="col-md-9">
						                    <input id="link_id" type="text" class="form-control" name="link_id" value="" required autofocus>
						                </div>
						                </div>
						                </div>
						        </div>
						    </div>
						    <div class="text-center">
						    	<input type="submit" value="Submit" class="btn btn-primary">
						    </div>
						    </div>
						    
						</form>
						@if (\Session::has('errors'))
						    <div class="alert alert-danger">
						    	{!! \Session::get('errors') !!}
						    </div>
						@endif


	</div>
</div>
</main>
</div>

@endsection