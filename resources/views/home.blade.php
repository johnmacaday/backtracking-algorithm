@extends('layouts.app')

@section('style')
  <link rel="stylesheet" href="{{ asset('css/pure-min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/grids-responsive-min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/marketing.css') }}">
  <style>
    .content-wrapper {
      left: 0;
    }
    .splash-container{
      background-image: url({{asset('img/Laptop.jpg')}});
      background-position: center;
      background-size: cover;
    }
  </style>
@endsection

@section('content')
<div class="container">

<div class="splash-container">
  <div class="splash">
      <h1 class="splash-head" style="font-weight: bold;text-shadow: 3px 3px #000000;">Web-Based Statistical System</h1>
      <p class="splash-subhead" style="text-shadow: 2px 2px #000000;">Create your own survey, have people answer and generate the results. Login or Register to start.</p>
      <p>
          <a href="{{route('login')}}" class="pure-button pure-button-primary">Get Started</a>
      </p>
  </div>
</div>

<div class="content-wrapper">
  <div class="content">
      <h2 class="content-head is-center">Features</h2>

      <div class="pure-g">
        <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
             <h3 class="content-subhead">Survey Maker</h3>
             <p>
 Users are able to make a survey and answer other people's survey with this system. A survey maker not only reduces the time required to take the survey, but also brings ease and flexibility in the entire process of creating, distributing, and analyzing a survey
</p>
         </div>
         <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
               <h3 class="content-subhead"> T-Test Calculator</h3>
               <p>The system uses T-test to calculate results to compare the user's proposed system and existing system.
<br> The T-test uses these two samples and compare their means to see their differences according to the users' opinions about other users' surveys.</p>
           </div>

          <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
              <h3 class="content-subhead">Graphs and Tables</h3>
              <p>
                  The system will help convert the computed data of the users to tables and graphs. This will help them do their statistical computation and representation easier.
              </p>
          </div>
          <div class="l-box pure-u-1 pure-u-md-1-2 pure-u-lg-1-4">
              <h3 class="content-subhead">
                  Statistical Analysis
              </h3>
              <p>
                  Before, during, or after the collection of your data, statistical support can be required. In order to assist you in solving your scientific questions, we provide statistical assistance in any part of your project. We can perform the analysis for you to help with the statistical problems for your documentations.
              </p>
          </div>
      </div>
  </div>

  <div class="ribbon l-box-lrg pure-g">
      <div class="l-box-lrg is-center pure-u-1 pure-u-md-1-2 pure-u-lg-2-5">
          <img width="300" alt="File Icons" class="pure-img-responsive" src="{{asset('img/map.png')}}">
      </div>
      <div class="pure-u-1 pure-u-md-1-2 pure-u-lg-3-5">
          <h2 class="content-head content-head-ribbon">Respondents Heatmap</h2>
          <p> The system shows where the people who answered your survey are. It shows a map of Quezon City and retrieves the location of the respondents. <br>The map feature uses Backtracking Algorithm.</p>
        </div>
  </div>

  <div class="content">
      <h2 class="content-head is-center">About this Site</h2>
      <div class="pure-g">
          <div class="l-box-lrg pure-u-1 pure-u-md-3-5">
              <h4>System Information</h4>
              <p>This system is made to help AMA students as a temporary statistician. The system is not meant to be a replacement for a statistician.</p>
          </div>
          <div class="l-box-lrg pure-u-1 pure-u-md-3-5">
            <h4>Backtracking Algorithm</h4>
    <p>Backtracking Algorithm’s idea is to assign colors one by one to different vertices, starting from the vertex 0. Before assigning a color, it checks for safety by considering already assigned colors to the adjacent vertices. <br> If it finds a color assignment which is safe, it marks the color assignment as part of solution. If it doesn’t a find color due to clashes, then it backtracks and return false. This algorithm is applied to the graph problem. For example, the world map can be considered as a graph problem. Every region or state can be considered as a vertex. </p>
                <h4>System Information</h4>
                <p>This system is made to help AMA students as a temporary statistician. The system is not meant to be a replacement for a statistician.</p>
            </div>
      </div>
  </div>
</div>
</div>
@endsection
