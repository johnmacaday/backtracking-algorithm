@extends('layouts.app')

@section('head-scripts')
	<script src="{{ asset('js/Chart.min.js') }}"></script>
@endsection

@section('style')
	<style>
		.tTestChart td, .tTestChart tr, .tTestChart th{
			border: 1px solid black;
		}
	</style>
@endsection

@section('content')
<div class="container mb-5 tTestChart">
 	<caption><h3>T-test of the Existing and the Proposed System</h3></caption>
	<table style="width:100%">

	<tr>
		<th>Criteria</th>
		<th>Mean of the Existing System</th>
		<th>Verbal Interpretation</th>
		<th>Mean of the Proposed System</th>
		<th>Verbal Interpretation</th>
	</tr>
	@foreach ($survey->criterias as $criteria)
	<tr>
		<th>{{$criteria->criteria}}</th>
		<td>{{number_format($criteria->totalValue->existingTotal, 3)}}</td>
		<td>Effective</td>
		<td>{{number_format($criteria->totalValue->proposedTotal, 3)}}</td>
		<td>Effective</td>
	</tr>
	@endforeach
	<tr>
		<th>Weighted Mean</th>
		<th>{{number_format($survey->stats->esMean, 3)}}</th>
		<th>Effective</th>
		<th>{{number_format($survey->stats->psMean,3)}}</th>
		<th>Effective</th>
	</tr>
</table>
</div>


	<div class="container tTestChart">
		  <caption><h3>T-test of the Existing and the Proposed System</h3></caption>
		<table style="width:100%">

  <tr>
    <th>System</th>
    <th>Weighted Mean</th>
    <th>Standard Deviation</th>
    <th>Total Number of Criteria</th>
  </tr>
  <tr>
    <td><h4>Existing</h4></td>
    <td>{{number_format($survey->stats->esMean, 3)}}</td>
    <td>{{number_format($survey->stats->eStd, 3)}}</td>
    <td>{{count($survey->criterias)}}</td>
  </tr>
   <tr>
    <td><h4>Proposed</h4></td>
    <td>{{number_format($survey->stats->psMean, 3)}}</td>
    <td>{{number_format($survey->stats->pStd, 3)}}</td>
    <td>{{count($survey->criterias)}}</td>
  </tr>
   <tr>
    <td><h4>T-Test Result</h4></td>
    <td colspan="3"><h4>{{number_format($survey->stats->tTest, 3)}}</h4></td>
    
  </tr>
  
</table>
	</div>

	<div class="container">
		<canvas id="myChart" width="400" height="200"></canvas>
	</div>
@endsection

@section('scripts')
<script>
var ctx = document.getElementById("myChart");
var blue = 'rgba(54, 162, 235, 0.2)';
var pink = 'rgba(255, 0, 255, 0.2)'
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
    datasets: [{
			label: 'Existing System',
			data: [	@foreach ($survey->criterias as $criteria)
							{!!number_format($criteria->totalValue->existingTotal, 2) . ","!!}
						@endforeach],
	      	backgroundColor: 'rgba(126,206,255, 0.3)',
		    borderColor: 'rgba(126,190,255, 1)',
		    borderWidth: 1
        },

         {
          label: 'Proposed System',
          data: [@foreach ($survey->criterias as $criteria)
						{!! number_format($criteria->totalValue->proposedTotal, 2) . ","!!}
					@endforeach],
          backgroundColor: 'rgba(126,158,255, 0.3)',
		    borderColor: 'rgba(126,142,255,1)',
		    borderWidth: 1

          // Changes this dataset to become a line
          // type: 'line'
        }],
    labels: [@foreach ($survey->criterias as $criteria)
						{!!"'".$criteria->criteria ."'". ","!!}
					@endforeach]
  },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }],
        },
        layout: {
            padding: {
                left: 50,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        legend: {
            labels: {
                // This more specific font property overrides the global property
                fontStyle: 'bold'
            }
        }
    },
});
</script>

@endsection