@extends('layouts.app')

@section('head-scripts')
	
	<script src="{{asset('js/highmaps.js')}}"></script>
	<script src="{{asset('js/exporting.js')}}"></script>
	<script src="{{asset('js/ph-all.js')}}"></script>

@endsection
	
@section('style')
	<link rel="stylesheet" href="{{asset('css/maps.css')}}">
@endsection

@section('content')
	<div id="map-container">
		
	</div>
@endsection

@section('scripts')
	
	<script>	

		var data = [
		    @for ($i = 0; $i < 17; $i++)
				{!! "['" . $cityList[$i] ."',"!!}
				@if ($loca[$i] > 0)
					{!! $loca[$i] ."," . "'" . $colors[$a[$i] - 1] ."'],"  !!}
				@else
					{!! 0 ."," . "'" . "#ffffff" ."'],"  !!}
				@endif
			@endfor
		];

		// Create the chart
		Highcharts.mapChart('map-container', {
		    chart: {
		        map: 'countries/ph/ph-all',
		        events: {
		        load: function () {
		            this.mapZoom(0.025, 1780, -5650);
		            }
		        }
		    },

		    title: {
		        text: 'Highmaps basic demo'
		    },

		    subtitle: {
		        text: 'Source map: <a href="http://code.highcharts.com/mapdata/countries/ph/ph-all.js">Philippines</a>'
		    },
		  
		    mapNavigation: {
		        enabled: false,
		        /* buttonOptions: {
		            verticalAlign: 'bottom'
		        } */
		    },

		    series: [{
		        data: data,
		        name: 'Heatmap',
		        keys: ['hc-key','value', 'color'],
		        states: {
		            hover: {
		                color: '#ffdfba',
		            }
		        },
		        dataLabels: {
		            enabled: true,
		            format: '{point.name}',
		            crop: false,
		            overflow: 'none'
		        }
		    }]
		});
</script>

@endsection