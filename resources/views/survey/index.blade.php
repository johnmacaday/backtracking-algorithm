@extends('layouts.app')

@section('content')
	<div class="container">
		<h1>Surveys</h1>
		<ul>
			@foreach ($surveys as $survey)
				<a href="/survey/{{ $survey->id }}">
					<li>{{ $survey->title }}</li>
				</a>
			@endforeach
		</ul>
	</div>
@endsection