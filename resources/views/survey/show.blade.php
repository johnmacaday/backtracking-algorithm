@extends('layouts.app')

@section('content')
	<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
					<div class="container">

		@if ($surveyBool)
			<div class="row">
			{{-- expr --}}
			@foreach ($latestSurveys as $survey)
				{{-- expr --}}
				<div class="card p-3 mx-auto mb-5 col-sm-5">
				<div>
					<h3 style="border-bottom: 1px solid lightgray;"><a href="{{url('answers/'.$survey->id)}}">{{ $survey->title }}</a></h3>
					<h5>Survey ID: {{$survey->id}}</h5>
					<h5>Date Created: {{$survey->created_at->toFormattedDateString()}}</h5>
					<h5>Total Respondents: {{$survey->answers()->distinct('user_id')->count('user_id')}}</h5>
				</div>
			</div>
			@endforeach
			</div>
		@else
			<div class="card mb-5 p-4">
				<div class="card p-3 mb-3 text-center">
					<h4>You have not created any survey</h4>
				</div>
			</div>
		@endif
	</div>
	</div>
</main>
</div>
@endsection
