@extends('layouts.app')

@section('style')
	
	<style>
		.close{
			font-size: 18px;
			opacity: .75 !important;
		}
		.close:hover{
			opacity: 1 !important;
		}
	</style>

@endsection

@section('content')
	<div class="row">
			@include('_includes.sidebar')

			<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
				<div style="width: 100%;"  id="dash-main">
	<div class="container mb-5">
		<div class="row justify-content-center">
	        <div class="col-md-12">
	        	<form action="{{ route('survey.store') }}" method="POST">
								@csrf
		            <div class="card">
		                <div class="card-header">
							<div class="row">
						                <label for="title" class="col-sm-1 col-form-label text-md-right">Title:</label>

						                <div class="col-md-8">
						                    <input id="title" type="text" class="form-control" name="title" value="" required autofocus>

						                    @if ($errors->has('title'))
						                        <span class="invalid-feedback" role="alert">
						                            <strong>{{ $errors->first('title') }}</strong>
						                        </span>
						                    @endif
						                </div>
						            </div>
		              	  </div>
	                	<div class="card-body" id="surveyBody">
	                		<div class="card">
	                			<div class="card-body" id="hehe123">
								<input id="hidden-id" type="hidden" name="qID[]" value ="0">
								<div class="form-group row">
					                <label for="question" class="col-sm-2 col-form-label text-md-right">Question: </label>
									
					                <div class="col-md-8">
					                    <input id="question" type="text" class="form-control" name="question[0]" value="" required>
					                    @if ($errors->has('question'))
					                        <span class="invalid-feedback" role="alert">
					                            <strong>{{ $errors->first('question') }}</strong>
					                        </span>
					                    @endif
					                </div>
					            </div>
					            <div class="form-group row">
					                <label for="choice" class="col-sm-3 col-form-label text-md-right">Choice: </label>

					                <div class="col-md-7">
					                    <input id="choice" type="text" class="form-control" name="choice[0][]" value="" required>
					                </div>
					            </div>
					             <div class="form-group row">
					                <label for="choice" class="col-sm-3 col-form-label text-md-right">Choice: </label>

					                <div class="col-md-7">
					                    <input id="choice" type="text" class="form-control" name="choice[0][]" value="" required>
					                </div>
					            </div>
								<div id="1" class="form-group row">
									<input type="button" value="Add Choice" class="addChoice col-sm-3 m-auto btn btn-secondary">
								</div>
	                			</div>
	                		</div>
		            <div class="container pt-4">
						<div class="form-group row">
							<input type="button" value="Add Question" class="addQuestion col-sm-3 btn btn-secondary ml-auto">
						</div>
					</div>
					</div>
				</div>

				<div class="card mt-3">
		                <div class="card-header">
							<h4>Criteria</h4>
		              	  </div>
	                	<div class="card-body" id="surveyBody">
	                		<div class="card">
	                			<div class="card-body" id="hehe123">
								<div class="form-group row">
					                <label for="criteria" class="col-sm-2 col-form-label text-md-right">Criteria: </label>
					                <div class="col-md-8">
					                    <input id="criteria" type="text" class="form-control" name="criteria[]" value="" required>
					                </div>
					            </div>
					            <div class="form-group row">
					                <label for="criteria" class="col-sm-2 col-form-label text-md-right">Criteria: </label>
					                <div class="col-md-8">
					                    <input id="criteria" type="text" class="form-control" name="criteria[]" value="" required>
					                </div>
					            </div>
								<div id="1" class="form-group row">
									<input type="button" value="Add Criteria" class="addCriteria col-sm-3 m-auto btn btn-secondary">
								</div>
	                			</div>
	                		</div>
					</div>
				</div>

				<div class="form-group row mb-0 pt-3 col-sm-12">
                    <div class="m-auto pt-3">
                        <button type="submit" class="btn btn-primary px-5">
                            {{ __('Submit') }}
                        </button>
                    </div>
		                </div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>


@endsection

@section('scripts')
	<script>
		$(document).ready(function() {
			
		
			$('body').on('click', '.addChoice', function() {
	  			var x = $(this).parent().siblings("#hidden-id").attr("value");
	  			var y = parseInt(x);

	  			$(this).parent().before(' <div class="form-group row"><button type="button" class="delChoice close text-danger text-left text-right col-sm-1 offset-md-1" aria-label="Close"><span aria-hidden="true">[×]</span></button><label for="choice" class="col-sm-1 col-form-label text-md-right">Choice: </label><div class="col-md-7"><input id="choice" type="text" class="form-control" name="choice[' + y + '][]" value="" required></div></div> ');
			});

			$('body').on('click', '.addCriteria', function() {

	  			$(this).parent().before(' <div class="form-group row"><button type="button" class="delCriteria close text-danger text-left text-right col-sm-1" aria-label="Close"><span aria-hidden="true">[×]</span></button><label for="criteria" class="col-sm-1 col-form-label text-md-right">Criteria: </label><div class="col-md-8"><input id="criteria" type="text" class="form-control" name="criteria[]" value="" required></div></div> ');
			});

			$(".addQuestion").click(function(){
	  			var x = $(this).parent().parent().prev(".card").children().children("#hidden-id").attr("value");
	  			var y = parseInt(x);
	  			y += 1;
	  			// alert(x);
	  			$(this).parent().parent().prev(".card").after('<div class="card mt-4"><button type="button" class="delQuestion close text-danger text-left mr-auto p-3" aria-label="Close"><span aria-hidden="true">× Remove</span></button><div class="card-body"><input id="hidden-id" type="hidden" name="qID[]" value =" ' + y + '"><div class="form-group row"><label for="question" class="col-sm-2 col-form-label text-md-right">Question: </label><div class="col-md-8"><input id="question" type="text" class="form-control" name="question['+ y +']" value="" required>' + '@if ($errors->has("question"))' + '<span class="invalid-feedback" role="alert"><strong>' + '{{ $errors->first('question') }}' + '</strong></span>' + '@endif' + '</div></div><div class="form-group row"><label for="choice" class="col-sm-3 col-form-label text-md-right">Choice: </label><div class="col-md-7"><input id="choice" type="text" class="form-control" name="choice['+ y +'][]" value="" required></div></div><div class="form-group row"><label for="choice" class="col-sm-3 col-form-label text-md-right">Choice: </label><div class="col-md-7"><input id="choice" type="text" class="form-control" name="choice['+ y +'][]" value="" required></div></div><div id="1" class="form-group row"><input type="button" value="Add Choice" class="addChoice col-sm-3 m-auto btn btn-secondary"></div></div></div>');
			});

			$('body').on('click', '.delChoice', function() {
				$(this).parent().replaceWith('<div class="alert alert-danger col-sm-6 mx-auto mb-2 text-center delAlert" role="alert">Choice successfully removed.</div>');
				$(".delAlert").fadeOut(2000, function(){ $(".delAlert").remove(); });
			});

			$('body').on('click', '.delCriteria', function() {
				$(this).parent().replaceWith('<div class="alert alert-danger col-sm-6 mx-auto mb-2 text-center delAlert" role="alert">Criteria successfully removed.</div>');
				$(".delAlert").fadeOut(2000, function(){ $(".delAlert").remove(); });
			});

			$('body').on('click', '.delQuestion', function() {
				$(this).parent().replaceWith('<div class="alert alert-danger col-sm-6 mx-auto my-3 text-center delAlert" role="alert">Question successfully removed.</div>');
				$(".delAlert").fadeOut(2000, function(){ $(".delAlert").remove(); });
			});

		});

	</script>
@endsection