@extends('layouts.app')

@section('style')
	<style>
		thead th {
		  border-top: 1px solid #dddddd;
		  border-bottom: 1px solid #dddddd;
		  border-right: 1px solid #dddddd;
		}
	</style>
@endsection

@section('content')
	<div class="container mb-5">
		<h1> {{ $survey->title }} </h1>
	</div>
{{-- 	{{ dd($survey->answers->count())}} --}}

	<form method="POST" action="{{url('survey/answer')}}">
		@csrf
		<div class="container mb-5">
		<label for="location" style="font-size: 16px"><h3>Where do you live?</h3></label>
		<select class="form-control form-control-md" name="location" id="locationSelect">
			@for ($i = 0; $i < 17; $i++)
				<option value="{!!number_format($i)!!}">{{$cities[$i]}}</option>
			@endfor
		</select>
		</div>
		<div class="container mb-5">
		<input type="hidden" name="survey" value="{{ number_format($survey->id) }}">
		<hidden>{{ $q = 0}}</hidden>
		@foreach ($survey->questions as $question)
			<div class="card p-3">
				<h3 class="mb-3" style="border-bottom: 1px solid lightgray;">{{ $question->question }}</h3>
					<input type="hidden" name="question[]" value="{{ number_format($question->id) }}">
					@foreach ($question->choices as $choice)
						<div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="answer[{{ number_format($q) }}][{{ number_format($choice->id) }}]" name="answer[{{ number_format($q) }}]" value="{{ number_format($choice->id) }}">
							<label class="custom-control-label" for="answer[{{ number_format($q) }}][{{ number_format($choice->id) }}]">{{ $choice->choice }}</label>
						</div>
					@endforeach
			</div>
			<hidden>{{ ++$q}}</hidden>
		@endforeach
		</div>

		<div class="container">
			<div class="card mb-5">
				<table style="width:100%" class="statsTable cell-border">
					<thead>
					  <tr>
					    <th >Criteria</th>
					    <th colspan="5">Existing System</th>
					    <th colspan="5">Proposed System</th>
					  </tr>
					</thead>
					<tbody>
						<tr>
					  	<td></td>
					    <th>Very Effective</th>
					    <th>Effective</th>
					    <th>Moderate</th>
					    <th>Slightly Effective</th>
					    <th>Not Effective</th>
							<th>Very Effective</th>
					    <th>Effective</th>
					    <th>Moderate</th>
					    <th>Slightly Effective</th>
					    <th>Not Effective</th>
					  </tr>
					  <tr>
					  	<td></td>
					    <th>5</th>
					    <th>4</th>
					    <th>3</th>
					    <th>2</th>
					    <th>1</th>
					    <th>5</th>
					    <th>4</th>
					    <th>3</th>
					    <th>2</th>
					    <th>1</th>
					  </tr>
						  <hidden>{{ $q = 0 }}</hidden>
						  @foreach ($survey->criterias as $criteria)
						  <tr>
							<input type="hidden" name="criteria[]" value={{$criteria->id}}>
							<th>{{ $criteria->criteria }}</th>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input"  id="existingValue[{{number_format($q)}}][5]" name="existingValue[{{number_format($q)}}]" value="5"><label class="custom-control-label" for="existingValue[{{number_format($q)}}][5]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input"  id="existingValue[{{number_format($q)}}][4]" name="existingValue[{{number_format($q)}}]" value="4"><label class="custom-control-label" for="existingValue[{{number_format($q)}}][4]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input"  id="existingValue[{{number_format($q)}}][3]" name="existingValue[{{number_format($q)}}]" value="3"><label class="custom-control-label" for="existingValue[{{number_format($q)}}][3]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="existingValue[{{number_format($q)}}][2]" name="existingValue[{{number_format($q)}}]" value="2"><label class="custom-control-label" for="existingValue[{{number_format($q)}}][2]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="existingValue[{{number_format($q)}}][1]" name="existingValue[{{number_format($q)}}]" value="1"><label class="custom-control-label" for="existingValue[{{number_format($q)}}][1]"></label></div></td>

							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="proposedValue[{{number_format($q)}}][5]" name="proposedValue[{{number_format($q)}}]" value="5"><label class="custom-control-label" for="proposedValue[{{number_format($q)}}][5]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="proposedValue[{{number_format($q)}}][4]" name="proposedValue[{{number_format($q)}}]" value="4"><label class="custom-control-label" for="proposedValue[{{number_format($q)}}][4]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="proposedValue[{{number_format($q)}}][3]" name="proposedValue[{{number_format($q)}}]" value="3"><label class="custom-control-label" for="proposedValue[{{number_format($q)}}][3]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="proposedValue[{{number_format($q)}}][2]" name="proposedValue[{{number_format($q)}}]" value="2"><label class="custom-control-label" for="proposedValue[{{number_format($q)}}][2]"></label></div></td>
							<td><div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="proposedValue[{{number_format($q)}}][1]" name="proposedValue[{{number_format($q)}}]" value="1"><label class="custom-control-label" for="proposedValue[{{number_format($q)}}][1]"></label></div></td>
					  </tr>
					  <hidden>{{ $q++ }}</hidden>
					  @endforeach
				  </tbody>
				</table>
			</div>
			<input class="btn btn-success" type="submit" value="Submit Answer">
		</div>



	</form>
@endsection

@section('scripts')
	<script>
	$(document).ready(function() {
		var select = $('#locationSelect');
		select.html(select.find('option').sort(function(x, y) {
		  // to change to descending order switch "<" for ">"
		  return $(x).text() > $(y).text() ? 1 : -1;
		}));
	});
</script>
<script>
	$('table.statsTable').DataTable( {
		        "paging":   false,
		        "ordering": false,
		        "info":     false,
		        "searching": false,
		    });
</script>
@endsection
