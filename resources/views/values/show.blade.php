@extends('layouts.app')
@section('style')

	<style>
		
		.tTestChart td, .tTestChart tr, .tTestChart th{
			border: 1px solid black;
		}

	</style>

@endsection
@section('content')
	
	{{-- {{ dd($survey->criterias->find(1)->proposedValues) }} --}}
	{{-- {{dd($pTotal)}} --}}


<div class="container mb-5 tTestChart">
 	<caption><h3>T-test of the Existing and the Proposed System</h3></caption>
	<table style="width:100%">

	<tr>
		<th>Criteria</th>
		<th>Mean of the Existing System</th>
		<th>Verbal Interpretation</th>
		<th>Mean of the Proposed System</th>
		<th>Verbal Interpretation</th>
	</tr>
	@foreach ($survey->criterias as $criteria)
	<tr>
		<th>{{$criteria->criteria}}</th>
		<td>{{number_format($criteria->totalValue->existingTotal, 3)}}</td>
		<td>Effective</td>
		<td>{{number_format($criteria->totalValue->proposedTotal, 3)}}</td>
		<td>Effective</td>
	</tr>
	@endforeach
	<tr>
		<th>Weighted Mean</th>
		<th>{{number_format($survey->stats->esMean, 3)}}</th>
		<th>Effective</th>
		<th>{{number_format($survey->stats->psMean,3)}}</th>
		<th>Effective</th>
	</tr>
</table>
</div>


	<div class="container tTestChart">
		  <caption><h3>T-test of the Existing and the Proposed System</h3></caption>
		<table style="width:100%">

  <tr>
    <th>System</th>
    <th>Weighted Mean</th>
    <th>Standard Deviation</th>
    <th>Total Number of Criteria</th>
  </tr>
  <tr>
    <td><h4>Existing</h4></td>
    <td>{{number_format($survey->stats->esMean, 3)}}</td>
    <td>{{number_format($survey->stats->eStd, 3)}}</td>
    <td>{{count($survey->criterias)}}</td>
  </tr>
   <tr>
    <td><h4>Proposed</h4></td>
    <td>{{number_format($survey->stats->psMean, 3)}}</td>
    <td>{{number_format($survey->stats->pStd, 3)}}</td>
    <td>{{count($survey->criterias)}}</td>
  </tr>
   <tr>
    <td><h4>T-Test Result</h4></td>
    <td colspan="3"><h4>{{number_format($survey->stats->tTest, 3)}}</h4></td>
    
  </tr>
  
</table>
	</div>
@endsection

@section('scripts')
	
	<script>

	</script>

@endsection